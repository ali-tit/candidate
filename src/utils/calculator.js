
export const distance = (firstPosition, secondPosition) => {
    let earthRadiusKm = 6371;

    let dLat = degreesToRadians(firstPosition.latitude-secondPosition.latitude)
    let dLon = degreesToRadians(firstPosition.longitude-secondPosition.longitude)

    let lat1 = degreesToRadians(firstPosition.latitude)
    let lat2 = degreesToRadians(secondPosition.latitude)

    let a = Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2)
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a))
    return Math.round(earthRadiusKm * c * 1000)
}

const  degreesToRadians = degrees =>{
    return degrees * Math.PI / 180;
}
