import {combineReducers} from "redux"
import pokemonReducer from './pokemonReducer'

const rootReducers = combineReducers({
    pokemonReducer : pokemonReducer
})

export default rootReducers