import React from 'react'
import {AppRegistry} from "react-native";
import AppNavigation from "./app.navigation";
import {Provider} from 'react-redux'
import store from "../store/store";



export default class CandidateApp extends React.PureComponent {


    render() {
        return (
            <Provider store={store}><AppNavigation/></Provider>
        )
    }

}

AppRegistry.registerComponent('component', () => CandidateApp)

