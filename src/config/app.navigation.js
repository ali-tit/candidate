import {StackNavigator} from 'react-navigation'
import Home from '../components/index/index'

const AppNavigation = StackNavigator(
    {
        main: {screen: Home}
    },
    {headerMode: 'none'}
)

export default AppNavigation