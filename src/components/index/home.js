import React from 'react'
import {
    View,
    Dimensions,
    Image,
    Platform,
    StatusBar,
    Animated,
    StyleSheet,
    PermissionsAndroid,
    ScrollView
} from "react-native"
import MapView, {Marker} from 'react-native-maps'
import Interactable from 'react-native-interactable'

const {width, height} = Dimensions.get('window')
const ASPECT_RATIO = width / height
const LATITUDE_DELTA = 0.0100;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO

const GEOLOCATION_OPTIONS = {
    enableHighAccuracy: Platform.OS === 'ios',
    timeout: 1000,
    distanceFilter: 20
}

export default class HomeComponent extends React.PureComponent {
    watchID = null

    constructor(props) {
        super(props)
        this._deltaY = new Animated.Value(height - 100);
        this.state = {
            userPosition: {
                latitude: 0,
                longitude: 0
            }
        }
    }

    componentDidMount() {

        if (Platform.OS === 'android')
            PermissionsAndroid
                .request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
                .then(granted => {
                    if (granted) this.watchLocation()
                })
        else
            this.watchLocation()

    }


    watchLocation() {
        this.watchID = navigator.geolocation
            .watchPosition(
                (position) => {
                    const userPosition = {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude
                    }
                    this.setState({userPosition: userPosition})
                },
                null,
                GEOLOCATION_OPTIONS
            )
    }

    componentWillUnmount() {
        if (this.watchID)
            navigator.geolocation.clearWatch(this.watchID)
    }


    render() {
        return (
            <View style={styles.container}>
                <MapView
                    style={styles.map}
                    showsUserLocation={true}
                    followsUserLocation={true}
                    userLocationAnnotationTitle={''}
                    showsPointsOfInterest={false}
                    showsCompass={false}
                    showsBuildings={true}
                    pitchEnabled={false}
                    rotateEnabled={false}
                    scrollEnabled={false}
                    moveOnMarkerPress={false}
                    toolbarEnabled={false}
                    zoomEnabled={false}
                    zoomControlEnabled={false}
                    region={{
                        latitudeDelta: LATITUDE_DELTA,
                        longitudeDelta: LONGITUDE_DELTA,
                        latitude: this.state.userPosition.latitude,
                        longitude: this.state.userPosition.longitude
                    }}>

                </MapView>

                <View style={styles.panelContainer} pointerEvents={'box-none'}>
                    <Animated.View
                        pointerEvents={'box-none'}
                        style={[styles.panelContainer, {
                            backgroundColor: 'black',
                            opacity: 0,
                        }]}/>
                    <Interactable.View
                        verticalOnly={true}
                        snapPoints={[{y: 70}, {y: height - 300}, {y: height - 100}]}
                        boundaries={{top: -300}}
                        initialPosition={{y: height - 200}}
                        animatedValueY={this._deltaY}>
                        <View style={styles.panel}>
                            <View style={styles.panelHandle}/>
                            <ScrollView horizontal={false} style={{width: width - 40}}>


                            </ScrollView>
                        </View>
                    </Interactable.View>
                </View>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#CCC',
        ...StyleSheet.absoluteFillObject,
    },
    panelContainer: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        zIndex: 3
    },
    panel: {
        height: height + 300,
        padding: 20,
        backgroundColor: 'white',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        shadowColor: '#000000',
        shadowOffset: {width: 0, height: 0},
        shadowRadius: 5,
        shadowOpacity: 0.4,
        height: '100%',
        flexDirection: 'column',
        alignItems: "center"

    },
    panelHeader: {
        alignItems: 'center',
        width: "100%",
        justifyContent: "space-between",
        flexDirection: 'row'
    },
    panelHandle: {
        width: 40,
        height: 8,
        borderRadius: 4,
        backgroundColor: '#00000040',
        marginBottom: 10
    },
    leftHandle: {
        width: width * (1 / 2) - 20,
        marginBottom: 10,
        flexDirection: 'row',
        justifyContent: "flex-start",
        alignItems: 'flex-start'
    },
    pickerLeft: {
        width: width * (2 / 8),
        backgroundColor: "#CCF5F5",
        height: 20,
    },
    pickeright: {
        width: width * (3 / 8) - 20,
        backgroundColor: "#CCC",
        height: 20,
    },
    filterText: {
        fontSize: 12,
        color: 'black',
        width: width * (1 / 8),
    },
    rightHandle: {
        width: width * (1 / 2) - 20,
        marginBottom: 10,
        flexDirection: 'row',
        justifyContent: "flex-end",
        alignItems: 'flex-end'
    },
    panelTitle: {
        fontSize: 27,
        height: 35
    },
    panelSubtitle: {
        fontSize: 14,
        color: 'gray',
        height: 30,
        marginBottom: 10
    },
    panelButton: {
        padding: 20,
        borderRadius: 10,
        backgroundColor: '#318bfb',
        alignItems: 'center',
        marginVertical: 10
    },
    panelButtonTitle: {
        fontSize: 17,
        fontWeight: 'bold',
        color: 'white'
    },
    map: {
        height: height,
        width: width,
        ...StyleSheet.absoluteFillObject,
    }
});