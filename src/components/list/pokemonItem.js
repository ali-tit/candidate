import React, {Component} from 'react'
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native'
import IconEntypo from 'react-native-vector-icons/Entypo'

export default class PokemonItem extends Component {

    constructor(props){
        super(props)
    }
    render() {
        return (
            <TouchableOpacity style={styles.item}>
                <View style={styles.itemHeader}>
                    <View style={styles.image}>
                        <Image resizeMode="cover" style={styles.image} source={{uri: "http://via.placeholder.com/250x250"}}/>
                    </View>
                    <View style={styles.itemInformation}>
                        <Text numberOfLines={1} style={styles.title}>Pokémon Name</Text>
                        <Text numberOfLines={1} style={styles.description}>Abilities :  Tough Claws</Text>
                    <Text numberOfLines={1} style={styles.description}>Weakness : Dragon, Rock, Ground"</Text>
                    </View>
                    <View style={styles.more}>
                        <IconEntypo name="direction" size={12} color="#CCC"/>
                        <Text style={[styles.textItemMore, {color: '#CCC'}]}> 120 m</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    item: {
        borderBottomWidth: 1,
        borderBottomColor: 'lightgrey',
        backgroundColor: 'white',
        padding: 10,
        borderLeftWidth: 4
    },
    itemHeader: {
        flex: 1,
        flexDirection: 'row'
    },
    image: {
        backgroundColor: 'white',
        borderRadius: 10,
        width : 60,
        height : 60
    },
    itemInformation: {
        marginLeft: 10,
        flex: 1,
        justifyContent: 'center',
        marginTop: -2,
        paddingRight: 10
    },
    title: {
        fontSize: 16,
        fontWeight: 'bold'
    },
    description: {
        fontSize: 12,
        color: '#666',
        flexWrap: 'wrap'
    },
    icon: {
        alignSelf: 'flex-start'
    },
    timelineContainer: {
        marginTop: 10,
        marginBottom: 10,
        height: 2,
        width: '100%',
        backgroundColor: 'lightgrey',
        borderRadius: 4
    },
    timelineProgress: {
        height: 2,
        borderRadius: 4
    },
    itemMore: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    more: {
        flexDirection: 'row',
        alignItems: 'baseline'
    },
    textItemMore: {
        fontSize: 12,
        marginLeft: 5
    }
})