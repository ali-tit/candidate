import { applyMiddleware, createStore, compose } from 'redux';
import thunk from 'redux-thunk';
import { logger } from 'redux-logger';
import rootReducers from '../reducers/root.reducer';

const store = createStore(rootReducers, applyMiddleware(logger), compose(applyMiddleware(thunk)));

export default store;
